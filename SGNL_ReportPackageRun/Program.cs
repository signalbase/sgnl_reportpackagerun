﻿using McMaster.Extensions.CommandLineUtils;
using System;
using System.Net.Http;
using System.Web;
using Newtonsoft.Json.Linq;
using System.Net.Http.Headers;

namespace SGNL_ReportPackageRun
{
    class Program
    {
        private static readonly HttpClient token = new HttpClient();
        private static readonly HttpClient reportPackage = new HttpClient();
        static int Main(string[] args)
        {
            var app = new CommandLineApplication();
            app.Name = "SGNL_ReportPackageRun";
            app.Description = "Executes a VPM ReportPackage. Created by Signalbase, Inc";
            app.HelpOption("-?|-h|--help");

            var loginUrlOption = app.Option("-l|--login", "URL for VPM Login Server", CommandOptionType.SingleValue);
            var environmentOption = app.Option("-e|--environment", "Environment code for VPM Server. This is case sensitive!", CommandOptionType.SingleValue);
            var usernameOption = app.Option("-u|--username", "Username to execute ReportPackage", CommandOptionType.SingleValue);
            var passwordOption = app.Option("-p|--password", "Password to execute ReportPackage", CommandOptionType.SingleValue);

            var automationUrlOption = app.Option("-a|--automation", "URL for VPM Automation Server", CommandOptionType.SingleValue);
            var rpIdOption = app.Option("-r|--reportPackageId", "Report Package Id", CommandOptionType.SingleValue);

            app.OnExecuteAsync(async r =>
            {
                if(loginUrlOption.HasValue() && environmentOption.HasValue() && usernameOption.HasValue() && passwordOption.HasValue() && automationUrlOption.HasValue() && rpIdOption.HasValue())
                { 
                    token.BaseAddress = new Uri(loginUrlOption.Value());
                    token.DefaultRequestHeaders.Clear();

                    var encodedUsername = HttpUtility.UrlEncode(usernameOption.Value());
                    var encodedPassword = HttpUtility.UrlEncode(passwordOption.Value());

                    var tokenContent = new StringContent("grant_type=password&login_type=Windows&ep_code=" + environmentOption.Value() + "&username=" + encodedUsername + "&password=" + encodedPassword);

                    HttpResponseMessage tokenResponse = await token.PostAsync("api/token", tokenContent);
                    try
                    {
                        tokenResponse.EnsureSuccessStatusCode();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Failed to get token, check options for errors.");
                        Console.WriteLine(e.ToString());
                    }

                    var x = JObject.Parse(await tokenResponse.Content.ReadAsStringAsync());
                    var accessToken = x["access_token"];

                    reportPackage.BaseAddress = new Uri(automationUrlOption.Value());
                    reportPackage.DefaultRequestHeaders.Clear();
                    reportPackage.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken.ToString());

                    HttpResponseMessage rpResponce = await reportPackage.PostAsync("api/jobs/" + rpIdOption.Value() + "/executions", new StringContent("")); 
                    try
                    {
                        rpResponce.EnsureSuccessStatusCode();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Failed to execute Report Package, check options for errors.");
                        Console.WriteLine(e.ToString());
                    }
                }
                else
                {
                    Console.WriteLine("Arguments are missing to execute Report Package. Run 'SGNL_ReportPackageRun -h' for more details.");
                }
            });

            return app.Execute(args);
        }
    }
}
